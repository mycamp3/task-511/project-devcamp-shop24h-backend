const orderDetailModel = require('../models/orderDetail.model');
const productModel = require('../models/product.model');
const mongoose = require('mongoose');

const createOrderDetail = async (req, res) => {
    const { productid, quantity } = req.body;

    if (!mongoose.Types.ObjectId.isValid(productid)) {
        return res.status(400).json({
            status: 'Bad request',
            message: 'Invalid product ID'
        });
    }

    try {
        const product = await productModel.findById(productid);
        if (!product) {
            return res.status(404).json({
                message: 'Product not found'
            });
        }

        const newOrderDetail = new orderDetailModel({
            product: productid,
            quantity: quantity || 1 // Default quantity is 1 if not specified
        });

        const savedOrderDetail = await orderDetailModel.create(newOrderDetail);
        // Populate the product field to include product details in the response
        const populatedOrderDetail = await orderDetailModel.findById(savedOrderDetail._id).populate('product');

        res.status(201).json({
            message: 'Order detail created successfully!',
            data: populatedOrderDetail
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error creating order detail'
        });
    }
};

const getAllOrderDetails = async (req, res) => {
    try {
        const orderDetails = await orderDetailModel.find();
        res.status(200).json({
            message: "Order details retrieved successfully!",
            data: orderDetails
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error fetching order details'
        });
    }
};

const getOrderDetailById = async (req, res) => {
    const orderdetailid = req.params.orderdetailid;

    if (!mongoose.Types.ObjectId.isValid(orderdetailid)) {
        return res.status(400).json({
            status: 'Bad request',
            message: 'Invalid order detail ID'
        });
    }

    try {
        const orderDetail = await orderDetailModel.findById(orderdetailid).populate('product');
        if (!orderDetail) {
            return res.status(404).json({
                message: 'Order detail not found'
            });
        }
        res.status(200).json(orderDetail);
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error fetching order detail'
        });
    }
};

const updateOrderDetailById = async (req, res) => {
    const orderdetailid = req.params.orderdetailid;
    const { productid, quantity } = req.body;

    if (!mongoose.Types.ObjectId.isValid(orderdetailid)) {
        return res.status(400).json({
            status: 'Bad request',
            message: 'Invalid order detail ID'
        });
    }

    if (productid && !mongoose.Types.ObjectId.isValid(productid)) {
        return res.status(400).json({
            status: 'Bad request',
            message: 'Invalid product ID'
        });
    }

    try {
        let orderDetail = await orderDetailModel.findById(orderdetailid);
        if (!orderDetail) {
            return res.status(404).json({
                message: 'Order detail not found'
            });
        }

        if (productid) {
            const product = await productModel.findById(productid);
            if (!product) {
                return res.status(404).json({
                    message: 'Product not found'
                });
            }
            orderDetail.product = productid;
        }

        if (quantity !== undefined) {
            orderDetail.quantity = quantity;
        }

        await orderDetail.save();
        const updatedOrderDetail = await orderDetailModel.findById(orderdetailid);

        res.status(200).json({
            message: 'Order detail updated successfully!',
            data: updatedOrderDetail
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error updating order detail'
        });
    }
};

const deleteOrderDetail = async (req, res) => {
    const orderdetailid = req.params.orderdetailid;

    if (!mongoose.Types.ObjectId.isValid(orderdetailid)) {
        return res.status(400).json({
            status: 'Bad request',
            message: 'Invalid order detail ID'
        });
    }

    try {
        await orderDetailModel.findByIdAndDelete(orderdetailid);
        
        res.status(200).json({
            message: 'Order detail deleted successfully!'
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error deleting order detail'
        });
    }
};

module.exports = {
    createOrderDetail,
    getAllOrderDetails,
    getOrderDetailById,
    updateOrderDetailById,
    deleteOrderDetail
};
