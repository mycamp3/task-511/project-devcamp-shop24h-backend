const Joi = require('joi');
const mongoose = require('mongoose');
const Order = require('../models/order.model');
const OrderDetail = require('../models/orderDetail.model'); // Assuming you have an OrderDetail model defined

const orderSchema = Joi.object({
    orderDate: Joi.date().default(Date.now).optional(),
    shippedDate: Joi.date().optional(),
    note: Joi.string().optional(),
    orderdetailid: Joi.string().required(),
    cost: Joi.number().default(0).optional()
});

const validateOrder = (order) => {
    return orderSchema.validate(order, { abortEarly: false });
};

const createOrder = async (req, res) => {
    const { orderDate, shippedDate, note, orderdetailid, cost } = req.body;

    // Validate request data
    const validationResult = validateOrder(req.body);
    const { error } = validationResult;

    if (error) {
        // Extract detailed error message
        const errorMessage = error.details.map((detail) => detail.message).join('; ');
        return res.status(400).json({
            status: 'Bad request',
            message: errorMessage
        });
    }

    // Validate orderdetailid format
    if (!mongoose.Types.ObjectId.isValid(orderdetailid)) {
        return res.status(400).json({
            status: 'Bad request',
            message: 'Invalid order detail ID format'
        });
    }

    try {
        // Check if the order detail exists
        const orderDetail = await OrderDetail.findById(orderdetailid);
        if (!orderDetail) {
            return res.status(404).json({
                message: 'Order detail not found'
            });
        }

        // Create a new order instance
        const newOrder = new Order({
            orderDate,
            shippedDate,
            note,
            orderDetails: [orderDetail._id], // Store orderdetailid in an array if needed
            cost
        });

        // Save the new order to the database
        const createdOrder = await Order.create(newOrder);
        //const populatedOrder = await Order.findById(createdOrder);
        res.status(201).json({
            message: 'Order created successfully!',
            data: createdOrder
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error creating order',
            error: err.message // Optionally include detailed error message
        });
    }
};


const getAllOrders = async (req, res) => {
    try {
        const orders = await Order.find().populate('orderDetails');
        res.status(200).json({
            message: 'Order retrieved successfully!',
            data: orders});
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error fetching orders',
            error: err.message // Optionally include detailed error message
        });
    }
};

const getOrderById = async (req, res) => {
    const orderid = req.params.orderid;

    try {
        const order = await Order.findById(orderid);
        if (!order) {
            return res.status(404).json({
                message: 'Order not found'
            });
        }
        res.status(200).json(order);
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error fetching order',
            error: err.message // Optionally include detailed error message
        });
    }
};

const updateOrderById = async (req, res) => {
    const orderid = req.params.orderid;
    const { orderDate, shippedDate, note, cost } = req.body;

    try {
        const order = await Order.findById(orderid);
        if (!order) {
            return res.status(404).json({
                message: 'Order not found'
            });
        }

        order.orderDate = orderDate || order.orderDate;
        order.shippedDate = shippedDate || order.shippedDate;
        order.note = note || order.note;
        order.cost = cost || order.cost;

        await order.save();

        const updatedOrder = await Order.findById(orderid).populate('orderDetails');
        res.status(200).json({
            message: 'Order updated successfully!',
            data: updatedOrder
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error updating order',
            error: err.message // Optionally include detailed error message
        });
    }
};

const deleteOrderById = async (req, res) => {
    const orderid = req.params.orderid;

    try {
        const order = await Order.findById(orderid);
        if (!order) {
            return res.status(404).json({
                message: 'Order not found'
            });
        }

        await order.deleteOne();
        res.status(200).json({
            message: 'Order deleted successfully!'
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error deleting order',
            error: err.message // Optionally include detailed error message
        });
    }
};

module.exports = {
    createOrder,
    getAllOrders,
    getOrderById,
    updateOrderById,
    deleteOrderById
};