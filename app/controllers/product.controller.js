const productModel = require('../models/product.model');
const productTypeModel = require('../models/productType.model');
const mongoose = require("mongoose");

const createProduct = async (req, res) => {

    // B1: Thu thập dl
    const { name, description, imageUrl, buyPrice, promotionPrice, amount, productTypeid } = req.body;

    //B2: Validate Dl
    if (!mongoose.Types.ObjectId.isValid(productTypeid)) {
        return res.status(400).json({
            status: "Bad request",
            message: "productTypeid not valid"
        });
    }

    if (!name || !imageUrl) {
        return res.status(400).json({
            message: "Invalid data"
        })
    }

    if (promotionPrice > buyPrice) {
        return res.status(400).json({
            message: "Promotion Price is invalid"
        })
    }

    if (promotionPrice < 0 && buyPrice < 0) {
        return res.status(400).json({
            message: "Prices are invalid"
        })
    }

    // B3: Xử lý DL
    try {
        const productType = await productTypeModel.findById(productTypeid);
        const newProduct = new productModel({ name, description, imageUrl, buyPrice, promotionPrice, amount, type:[productType._id] });
        const result = await productModel.create(newProduct);

        return res.status(201).json({
            message: 'Product type created successfully!',
            data: result
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error creating product type'
        });
    }
};

const getAllProducts = async (req, res) => {
    try {
        const products = await productModel.find().populate('type');
        res.status(200).json(products);
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error fetching products'
        });
    }
};

const getProductById = async (req, res) => {
    const productid = req.params.productid;

    if (!mongoose.Types.ObjectId.isValid(productid)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Invalid product ID"
        });
    }

    try {
        const product = await productModel.findById(productid);
        if (!product) {
            return res.status(404).json({
                message: "Product not found"
            });
        }
        res.status(200).json(product);
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error fetching product'
        });
    }
};

const updateProductById = async (req, res) => {
    const productid = req.params.productid;
    const { name, description, imageUrl, buyPrice, promotionPrice, amount, productTypeid } = req.body;

    if (!mongoose.Types.ObjectId.isValid(productid)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Invalid product ID"
        });
    }

    if (productTypeid && !mongoose.Types.ObjectId.isValid(productTypeid)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Invalid productTypeid"
        });
    }

    try {
        const product = await productModel.findById(productid);
        if (!product) {
            return res.status(404).json({
                message: "Product not found"
            });
        }

        if (name !== undefined) product.name = name;
        if (description !== undefined) product.description = description;
        if (imageUrl !== undefined) product.imageUrl = imageUrl;
        if (buyPrice !== undefined) product.buyPrice = buyPrice;
        if (promotionPrice !== undefined) product.promotionPrice = promotionPrice;
        if (amount !== undefined) product.amount = amount;
        if (productTypeid !== undefined) product.type = productTypeid;

        await product.save();
        const updatedProduct = await productModel.findById(productid).populate('type');

        res.status(200).json({
            message: 'Product updated successfully!',
            data: updatedProduct
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error updating product'
        });
    }
};

const deleteProduct = async (req, res) => {
    const productid = req.params.productid;

    if (!mongoose.Types.ObjectId.isValid(productid)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Invalid product ID"
        });
    }

    try {
        await productModel.findByIdAndDelete(productid);
        

        res.status(200).json({
            message: 'Product deleted successfully!'
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error deleting product'
        });
    }
};

module.exports = {
    createProduct,
    getAllProducts,
    getProductById,
    updateProductById,
    deleteProduct
};
