const productTypeModel = require('../models/productType.model');
const mongoose = require("mongoose");

const createProductType = async (req, res) => {

    // B1: Thu thập dl
    const { name, description } = req.body;

    //B2: Validate Dl
    if (!name) {
        return res.status(400).json({
            message: "Invalid name"
        })
    }

    // B3: Xử lý DL
    try {

        const newProductType = new productTypeModel({ name, description });
        const result = await productTypeModel.create(newProductType);

        return res.status(201).json({
            message: 'Product type created successfully!',
            data: result
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error creating product type'
        });
    }
};

// GetAllProductType controller
const getAllProductTypes = async (req, res) => {
    try {
        const productTypes = await productTypeModel.find();
        res.status(200).send({ message: 'Product types retrieved successfully!', data: productTypes });
    } catch (err) {
        console.error(err);
        res.status(500).send({ message: 'Error retrieving product types' });
    }
};

// GetProductTypeById controller
const getProductTypeById = async (req, res) => {
    
    //B1: Thu thập DL
    const productTypeid = req.params.productTypeid;
    //B2: Validate
    if(!mongoose.Types.ObjectId.isValid(productTypeid)) {
        return res.status(400).json({
            message: "ProductTypeid ko hợp lệ"
        }) 
    }
    try {
      
      const result = await productTypeModel.findById(productTypeid);
  
      if (!result) {
        return res.status(404).send({ message: 'Product type not found' });
      }
  
      res.status(200).send({ message: 'Product type retrieved successfully!', data: result });
    } catch (err) {
      console.error(err);
      res.status(500).send({ message: 'Error retrieving product type' });
    }
  };
  
  // UpdateProductType controller
 const updateProductTypeById = async (req, res) => {
    //B1: Thu thập DL
    const productTypeid = req.params.productTypeid;
    const { name, description } = req.body;
    
    // B2: Validate
    if(!mongoose.Types.ObjectId.isValid(productTypeid)) {
        return res.status(400).json({
            message: "ProductTypeid not valid"
        }) 
    }
    if (!name) {
        return res.status(400).json({
            message: "Invalid name"
        })
    }
    try {
      
      const productType = await productTypeModel.findByIdAndUpdate(productTypeid, { name, description }, { new: true });
  
      if (!productType) {
        return res.status(404).send({ message: 'Product type not found' });
      }
  
      res.status(200).send({ message: 'Product type updated successfully!', data: productType });
    } catch (err) {
      console.error(err);
      res.status(500).send({ message: 'Error updating product type' });
    }
  };
  
  // DeleteProductType controller
const deleteProductType = async (req, res) => {
    try {
      const productTypeid = req.params.productTypeid;
  
      await productTypeModel.findByIdAndDelete(productTypeid);
  
      res.status(200).send({ message: 'Product type deleted successfully!' });
    } catch (err) {
      console.error(err);
      res.status(500).send({ message: 'Error deleting product type' });
    }
  };

module.exports = {
    createProductType,
    getAllProductTypes,
    getProductTypeById,
    updateProductTypeById,
    deleteProductType
}