const Joi = require('joi');
const mongoose = require('mongoose');
const Customer = require('../models/customer.model');

const customerSchema = Joi.object({
    fullName: Joi.string().required(),
    phone: Joi.string().required(),
    email: Joi.string().email().required(),
    address: Joi.string().optional(),
    city: Joi.string().optional(),
    country: Joi.string().optional(),
    orders: Joi.array().items(Joi.string().required()).optional() // Accepts an array of order IDs (strings)
});

const validateCustomer = (customer) => {
    return customerSchema.validate(customer, { abortEarly: false });
};

const createCustomer = async (req, res) => {
    const { fullName, phone, email, address, city, country, orders } = req.body;

    // Validate request data
    const validationResult = validateCustomer(req.body);
    const { error } = validationResult;

    if (error) {
        // Extract detailed error message
        const errorMessage = error.details.map((detail) => detail.message).join('; ');
        return res.status(400).json({
            status: 'Bad request',
            message: errorMessage
        });
    }

    try {
        // Check if the customer already exists
        let existingCustomer = await Customer.findOne({ email });
        if (existingCustomer) {
            return res.status(400).json({
                message: 'Customer already exists'
            });
        }

        // Create a new customer instance
        const newCustomer = new Customer({
            fullName,
            phone,
            email,
            address,
            city,
            country,
            orders: orders || [] // Default to an empty array if orders are not provided
        });

        // Save the new customer to the database
        const createdCustomer = await newCustomer.save();
        res.status(201).json({
            message: 'Customer created successfully!',
            data: createdCustomer
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error creating customer',
            error: err.message // Optionally include detailed error message
        });
    }
};

// Controller function to get all customers
const getAllCustomers = async (req, res) => {
    try {
        const customers = await Customer.find();
        res.status(200).json({
            message: "Customers retrieved successfully!",
            data: customers});
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error retrieving customers',
            error: err.message
        });
    }
};

// Controller function to get a customer by ID
const getCustomerById = async (req, res) => {
    const customerid = req.params.customerid;

    try {
        const customer = await Customer.findById(customerid);
        
        if (!customer) {
            return res.status(404).json({
                message: 'Customer not found'
            });
        }
        
        res.status(200).json(customer);
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error retrieving customer',
            error: err.message
        });
    }
};

// Controller function to update a customer by ID
const updateCustomerById = async (req, res) => {
    const customerid = req.params.customerid;
    const updates = req.body;

    // Validate updated customer data
    const validationResult = validateCustomer(updates);
    const { error } = validationResult;

    if (error) {
        const errorMessage = error.details.map((detail) => detail.message).join('; ');
        return res.status(400).json({
            status: 'Bad request',
            message: errorMessage
        });
    }

    try {
        const updatedCustomer = await Customer.findByIdAndUpdate(customerid, updates, { new: true });

        if (!updatedCustomer) {
            return res.status(404).json({
                message: 'Customer not found'
            });
        }

        res.status(200).json({
            message: 'Customer updated successfully!',
            data: updatedCustomer
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error updating customer',
            error: err.message
        });
    }
};

// Controller function to delete a customer by ID
const deleteCustomerById = async (req, res) => {
    const customerid = req.params.customerid;

    try {
        const deletedCustomer = await Customer.findByIdAndDelete(customerid);

        if (!deletedCustomer) {
            return res.status(404).json({
                message: 'Customer not found'
            });
        }

        res.status(200).json({
            message: 'Customer deleted successfully!',
            data: deletedCustomer
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error deleting customer',
            error: err.message
        });
    }
};

module.exports = {
    createCustomer,
    getAllCustomers,
    getCustomerById,
    updateCustomerById,
    deleteCustomerById
};