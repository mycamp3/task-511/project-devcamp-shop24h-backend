const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Định nghĩa schema cho Product với timestamps
const productSchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    description: {
        type: String
    },
    type: {
        type: mongoose.Types.ObjectId,
        ref: 'ProductType',
        required: true
    },
    imageUrl: {
        type: String,
        required: true
    },
    buyPrice: {
        type: Number,
        required: true
    },
    promotionPrice: {
        type: Number,
        required: true
    },
    amount: {
        type: Number,
        default: 0
    }
}, {
    timestamps: true // Thêm timestamps
});

// Tạo model cho Product
const Product = mongoose.model('Product', productSchema);

module.exports = Product;
