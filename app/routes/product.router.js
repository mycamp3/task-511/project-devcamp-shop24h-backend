const express = require("express");
const router = express.Router();

const productController = require('../controllers/product.controller');

router.post("/", productController.createProduct);
router.get("/", productController.getAllProducts);
router.get("/:productid", productController.getProductById);
router.put("/:productid", productController.updateProductById);
router.delete("/:productid", productController.deleteProduct)

module.exports = router;