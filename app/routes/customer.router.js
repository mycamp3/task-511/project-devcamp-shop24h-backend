const express = require('express');
const router = express.Router();

const customerController = require('../controllers/customer.controller');

// Define routes for Customer operations
router.post('/', customerController.createCustomer);
router.get('/', customerController.getAllCustomers);
router.get('/:customerid', customerController.getCustomerById);
router.put('/:customerid', customerController.updateCustomerById);
router.delete('/:customerid', customerController.deleteCustomerById);

module.exports = router;
