const express = require("express");
const router = express.Router();

const orderDetailController = require('../controllers/orderDetail.controller');

// Định nghĩa các route và gắn với các hàm xử lý tương ứng từ controller
router.post("/", orderDetailController.createOrderDetail);
router.get("/", orderDetailController.getAllOrderDetails);
router.get("/:orderdetailid", orderDetailController.getOrderDetailById);
router.put("/:orderdetailid", orderDetailController.updateOrderDetailById);
router.delete("/:orderdetailid", orderDetailController.deleteOrderDetail);

module.exports = router;
