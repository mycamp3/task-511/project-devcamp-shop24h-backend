const express = require("express");
const router = express.Router();

const productTypeController = require('../controllers/productType.controller');

router.post("/", productTypeController.createProductType);
router.get("/", productTypeController.getAllProductTypes);
router.get("/:productTypeid", productTypeController.getProductTypeById);
router.put("/:productTypeid", productTypeController.updateProductTypeById);
router.delete("/:productTypeid", productTypeController.deleteProductType)

module.exports = router;
