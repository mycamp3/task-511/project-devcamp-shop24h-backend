const express = require('express');
const router = express.Router();

const orderController = require('../controllers/order.controller');

router.post('/', orderController.createOrder);
router.get('/', orderController.getAllOrders);
router.get('/:orderid', orderController.getOrderById);
router.put('/:orderid', orderController.updateOrderById);
router.delete('/:orderid', orderController.deleteOrderById);

module.exports = router;