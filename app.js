const express = require('express');
const app = express();
const port = 8000;
const mongoose = require('mongoose');

// Import model
const productTypeRouter = require('./app/routes/productType.router');
const productRouter = require('./app/routes/product.router');
const orderDetailRouter = require('./app/routes/orderDetail.router');
const orderRouter = require('./app/routes/order.router');
const customerRouter = require("./app/routes/customer.router");
//middleware
app.use(express.json());

// khai báo kết nối mongoDB qua mongoose
mongoose.connect("mongodb://127.0.0.1:27017/devcamp-shop24h-backend")
    .then(() => {
        console.log("MongoDB connected");
    })
    .catch((err) => {
        console.log(err);
    });

// Routes
app.get('/', (req, res) => {
    res.send('Welcome to Devcamp Shop 24h Backend!');
});

app.use("/devcamp-shop24h/productTypes", productTypeRouter);
app.use("/devcamp-shop24h/products", productRouter);
app.use("/devcamp-shop24h/orderdetails", orderDetailRouter);
app.use("/devcamp-shop24h/orders", orderRouter);
app.use("/devcamp-shop24h/customers", customerRouter);

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
})

module.exports = app