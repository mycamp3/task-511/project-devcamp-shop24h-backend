const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require("../app");
const mongoose = require('mongoose');
const ProductType = require("../app/models/productType.model");

const should = chai.should();

chai.use(chaiHttp);

describe("Test Devcamp Shop 24h - productType API", () => {
    // đảm bảo database rỗng trước khi test
    before(async () => {
        await mongoose.connect('mongodb://127.0.0.1:27017/devcamp-shop24h-backend');
        mongoose.connection.on('error', (e) => {
            if (e.message.code === 'ETIMEOUT') {
                console.log(e);
                mongoose.connect('mongodb://127.0.0.1:27017/devcamp-shop24h-backend');
            }
            console.log(e);
        });
        // xóa collection trước khi test
        await ProductType.deleteMany({});
    });

    after(async () => {
        //Disconnect from database sau khi hoàn thành tất cả các test
        await mongoose.connection.close();
    });

    describe("GET /devcamp-shop24h/productTypes", () => {
        it("Should get all product types", (done) => {
            chai.request(server)
                .get("/devcamp-shop24h/productTypes")
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a("object");
                    res.body.should.have.property("message").eql("Product types retrieved successfully!");
                    res.body.data.should.be.a("array");
                    done();
                });
        });
    });

    describe("POST /devcamp-shop24h/productTypes", () => {
        it("Should create a new product type", (done) => {
            const productType = {
                name: "Electronics",
                description: "Devices and gadgets"
            };

            chai.request(server)
                .post("/devcamp-shop24h/productTypes")
                .send(productType)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a("object");
                    res.body.should.have.property("message").eql("Product type created successfully!");
                    res.body.data.should.have.property("name").eql("Electronics");
                    res.body.data.should.have.property("description").eql("Devices and gadgets");
                    done();
                });
        });

    });

    describe("GET /devcamp-shop24h/productTypes/:productTypeid", () => {
        let productTypeid;

        before(async () => {
            const productType = new ProductType({
                name: "Books",
                description: "Various kinds of books"
            });
            const savedProductType = await productType.save();
            productTypeid = savedProductType._id;
        });

        it("Should return a product type by ID", (done) => {
            chai.request(server)
                .get(`/devcamp-shop24h/productTypes/${productTypeid}`)
                .end((err, res) => {
                    if (err) done(err);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('data');
                    res.body.data.should.have.property('name').eql('Books');
                    res.body.data.should.have.property('description').eql('Various kinds of books');
                    done();
                });
        });
    });

    describe("PUT /devcamp-shop24h/productTypes/:productTypeid", () => {
        let productTypeid;

        before(async () => {
            const productType = new ProductType({
                name: "Clothes",
                description: "Apparel and accessories"
            });
            const savedProductType = await productType.save();
            productTypeid = savedProductType._id;
        });

        it("Should update a product type by ID", (done) => {
            chai.request(server)
                .put(`/devcamp-shop24h/productTypes/${productTypeid}`)
                .send({
                    name: "Clothing",
                    description: "All types of clothes"
                })
                .end((err, res) => {
                    if (err) done(err);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('data');
                    res.body.data.should.have.property('name').eql('Clothing');
                    res.body.data.should.have.property('description').eql('All types of clothes');
                    done();
                });
        });
    });

    describe("DELETE /devcamp-shop24h/productTypes/:productTypeid", () => {
        let productTypeId;

        before(async () => {
            const productType = new ProductType({
                name: "Toys",
                description: "Children's toys"
            });
            const savedProductType = await productType.save();
            productTypeId = savedProductType._id;
        });

        it("Should delete a product type by ID", async () => {
            console.log(`Starting delete test for productTypeId: ${productTypeId}`);

            const res = await chai.request(server)
                .delete(`/devcamp-shop24h/productTypes/${productTypeId}`);

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('Product type deleted successfully!');

            // Verify that the product type was actually deleted
            const productType = await ProductType.findById(productTypeId);
            should.not.exist(productType);

            console.log(`Completed delete test for productTypeId: ${productTypeId}`);
        });
    });
});
