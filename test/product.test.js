const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const mongoose = require('mongoose');
const Product = require('../app/models/product.model');
const ProductType = require('../app/models/productType.model');

const should = chai.should();

chai.use(chaiHttp);

describe("Test Devcamp Shop 24h - Product API", () => {
    let productTypeId;

    // Ensure the database is clean before running tests
    before(async () => {
        try {
            await mongoose.connect('mongodb://127.0.0.1:27017/devcamp-shop24h-backend', {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            });

            mongoose.connection.on('error', (error) => {
                console.error('MongoDB connection error:', error);
            });

            console.log('MongoDB connected successfully.');

            await Product.deleteMany({});
            await ProductType.deleteMany({});

            const dummyProductType = new ProductType({
                name: "Electronics",
                description: "Electronic devices"
            });
            const savedProductType = await dummyProductType.save();
            productTypeId = savedProductType._id;

            console.log('Dummy product type created:', savedProductType);
            console.log('Product Type ID:', productTypeId);

        } catch (error) {
            console.error('Error in before hook:', error);
        }
    });

    after(async () => {
        // Disconnect from the database after all tests
        await mongoose.connection.close();
    });

    describe("POST /devcamp-shop24h/products", () => {
        it("Should create a new product", (done) => {
            chai.request(server)
                .post("/devcamp-shop24h/products")
                .send({
                    name: "Smartphone",
                    description: "Latest model smartphone",
                    imageUrl: "http://example.com/smartphone.jpg",
                    buyPrice: 500,
                    promotionPrice: 450,
                    amount: 100,
                    productTypeid: productTypeId.toString()
                })
                .end((err, res) => {
                    if (err) {
                        console.log('POST /products Error:', err.message);
                        done(err);
                    } else {
                        res.should.have.status(201);
                        res.body.should.be.a('object');
                        res.body.should.have.property('data');
                        res.body.data.should.have.property('name').eql('Smartphone');
                        res.body.data.should.have.property('description').eql('Latest model smartphone');
                        res.body.data.should.have.property('imageUrl').eql('http://example.com/smartphone.jpg');
                        res.body.data.should.have.property('buyPrice').eql(500);
                        res.body.data.should.have.property('promotionPrice').eql(450);
                        res.body.data.should.have.property('amount').eql(100);
                        res.body.data.should.have.property('type').eql(productTypeId.toString());
                        done();
                    }
                });
        });
    });

    describe("GET /devcamp-shop24h/products", () => {
        it("Should return an array of all products", (done) => {
            chai.request(server)
                .get("/devcamp-shop24h/products")
                .end((err, res) => {
                    if (err) {
                        console.log('GET /products Error:', err.message);
                        done(err);
                    } else {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.above(0);
                        res.body[0].should.have.property('name');
                        res.body[0].should.have.property('description');
                        res.body[0].should.have.property('imageUrl');
                        res.body[0].should.have.property('buyPrice');
                        res.body[0].should.have.property('promotionPrice');
                        res.body[0].should.have.property('amount');
                        res.body[0].should.have.property('type');
                        done();
                    }
                });
        });
    });

    describe("GET /devcamp-shop24h/products/:productid", () => {
        let productId;

        before(async () => {
            // Create a product for this test
            const product = new Product({
                name: "Tablet",
                description: "Latest model tablet",
                imageUrl: "http://example.com/tablet.jpg",
                buyPrice: 300,
                promotionPrice: 250,
                amount: 50,
                type: productTypeId
            });
            const savedProduct = await product.save();
            productId = savedProduct._id;
        });

        it("Should return a product by ID", (done) => {
            chai.request(server)
                .get(`/devcamp-shop24h/products/${productId}`)
                .end((err, res) => {
                    if (err) {
                        console.log('GET /products/:productid Error:', err.message);
                        done(err);
                    } else {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('name').eql('Tablet');
                        res.body.should.have.property('description').eql('Latest model tablet');
                        res.body.should.have.property('imageUrl').eql('http://example.com/tablet.jpg');
                        res.body.should.have.property('buyPrice').eql(300);
                        res.body.should.have.property('promotionPrice').eql(250);
                        res.body.should.have.property('amount').eql(50);
                        res.body.should.have.property('type').eql(productTypeId.toString());
                        done();
                    }
                });
        });
    });

    describe("PUT /devcamp-shop24h/products/:productid", () => {
        let productId;

        before(async () => {
            // Create a product for this test
            const product = new Product({
                name: "Laptop",
                description: "Gaming laptop",
                imageUrl: "http://example.com/laptop.jpg",
                buyPrice: 1000,
                promotionPrice: 900,
                amount: 20,
                type: productTypeId
            });
            const savedProduct = await product.save();
            productId = savedProduct._id;
        });

        it("Should update a product by ID", (done) => {
            chai.request(server)
                .put(`/devcamp-shop24h/products/${productId}`)
                .send({
                    name: "Gaming Laptop",
                    description: "High performance gaming laptop",
                    buyPrice: 1200,
                    promotionPrice: 1000
                })
                .end((err, res) => {
                    if (err) {
                        console.log('PUT /products/:productid Error:', err.message);
                        done(err);
                    } else {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('data');
                        res.body.data.should.have.property('name').eql('Gaming Laptop');
                        res.body.data.should.have.property('description').eql('High performance gaming laptop');
                        res.body.data.should.have.property('buyPrice').eql(1200);
                        res.body.data.should.have.property('promotionPrice').eql(1000);
                        done();
                    }
                });
        });
    });

    describe("DELETE /devcamp-shop24h/products/:productid", () => {
        let productId;

        before(async () => {
            const product = new Product({
                name: "Camera",
                description: "DSLR camera",
                imageUrl: "http://example.com/camera.jpg",
                buyPrice: 800,
                promotionPrice: 750,
                amount: 30,
                type: productTypeId
            });
            const savedProduct = await product.save();
            productId = savedProduct._id;
        });

        it("Should delete a product by ID", async () => {
            console.log(`Starting delete test for productId: ${productId}`);

            const res = await chai.request(server)
                .delete(`/devcamp-shop24h/products/${productId}`);

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('Product deleted successfully!');

            // Verify that the product type was actually deleted
            const product = await Product.findById(productId);
            should.not.exist(product);

            console.log(`Completed delete test for productId: ${productId}`);
        });
    });
});
