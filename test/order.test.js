const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const mongoose = require('mongoose');
const Order = require('../app/models/order.model');
const OrderDetail = require('../app/models/orderDetail.model');
const Product = require('../app/models/product.model');
const ProductType = require('../app/models/productType.model');

const should = chai.should();

chai.use(chaiHttp);

describe("Test Devcamp Shop 24h - Order API", () => {
    let productId;
    let orderDetailId;
    let orderId;

    // Ensure the database is clean before running tests
    before(async () => {
        try {
            await mongoose.connect('mongodb://127.0.0.1:27017/devcamp-shop24h-backend', {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            });

            mongoose.connection.on('error', (error) => {
                console.error('MongoDB connection error:', error);
            });

            console.log('MongoDB connected successfully.');

            await Order.deleteMany({});
            await OrderDetail.deleteMany({});
            await Product.deleteMany({});
            await ProductType.deleteMany({});

            // Create a dummy ProductType
            const dummyProductType = new ProductType({
                name: "Electronics",
                description: "Electronic devices"
            });
            const savedProductType = await dummyProductType.save();

            console.log('Dummy product type created:', savedProductType);

            // Create a dummy Product
            const dummyProduct = new Product({
                name: "Smartphone",
                description: "Latest model smartphone",
                imageUrl: "http://example.com/smartphone.jpg",
                buyPrice: 500,
                promotionPrice: 450,
                amount: 100,
                type: savedProductType._id
            });
            const savedProduct = await dummyProduct.save();
            productId = savedProduct._id;

            console.log('Dummy product created:', savedProduct);
            console.log('Product ID:', productId);

            // Create a dummy OrderDetail
            const orderDetail = new OrderDetail({
                product: productId,
                quantity: 2
            });
            const savedOrderDetail = await orderDetail.save();
            orderDetailId = savedOrderDetail._id;

            console.log('Dummy order detail created:', savedOrderDetail);

        } catch (error) {
            console.error('Error in before hook:', error);
        }
    });

    after(async () => {
        // Disconnect from the database after all tests
        await mongoose.connection.close();
    });

    afterEach(async () => {
        // Delete all Orders after each test
        await Order.deleteMany({});
    });

    describe("POST /devcamp-shop24h/orders", () => {
        it("Should create a new order", (done) => {
            chai.request(server)
                .post("/devcamp-shop24h/orders")
                .send({
                    orderDate: new Date(),
                    shippedDate: "2024-07-15",
                    note: "Test order",
                    orderdetailid: orderDetailId.toString(),
                    cost: 450
                })
                .end((err, res) => {
                    if (err) {
                        console.log('POST /orders Error:', err.message);
                        done(err);
                    } else {
                        res.should.have.status(201);
                        res.body.should.be.a('object');
                        res.body.should.have.property('message').eql('Order created successfully!');
                        res.body.should.have.property('data');
                        res.body.data.should.have.property('orderDetails').with.lengthOf(1);
                        res.body.data.should.have.property('note').eql('Test order');
                        res.body.data.should.have.property('cost').eql(450);                  
                        done();
                    }
                });
        });
    });

    describe("GET /devcamp-shop24h/orders", () => {
        it("Should return an array of all orders", (done) => {
            chai.request(server)
                .get("/devcamp-shop24h/orders")
                .end((err, res) => {
                    if (err) {
                        console.log('GET /orders Error:', err.message);
                        done(err);
                    } else {
                        res.should.have.status(200);
                        res.body.data.should.be.a('array');
                        res.body.should.have.property("message").eql("Order retrieved successfully!");
                        done();
                    }
                });
        });
    });

    describe("GET /devcamp-shop24h/orders/:orderid", () => {
        before(async () => {
            // Create an order for this test
            const order = new Order({
                orderDate: new Date(),
                shippedDate: null,
                note: "Test order",
                orderDetails: [orderDetailId],
                cost: 450
            });
            const savedOrder = await order.save();
            orderId = savedOrder._id;
        });

        it("Should return an order by ID", (done) => {
            chai.request(server)
                .get(`/devcamp-shop24h/orders/${orderId}`)
                .end((err, res) => {
                    if (err) {
                        console.log('GET /orders/:orderid Error:', err.message);
                        done(err);
                    } else {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('orderDate');
                        res.body.should.have.property('orderDetails');
                        done();
                    }
                });
        });
    });

    describe("PUT /devcamp-shop24h/orders/:orderid", () => {
        before(async () => {
            // Create an order for this test
            const order = new Order({
                orderDate: new Date(),
                shippedDate: null,
                note: "Test order",
                orderDetails: [orderDetailId],
                cost: 450
            });
            const savedOrder = await order.save();
            orderId = savedOrder._id;
        });

        it("Should update an order by ID", (done) => {
            chai.request(server)
                .put(`/devcamp-shop24h/orders/${orderId}`)
                .send({
                    note: "Updated test order",
                    cost: 500
                })
                .end((err, res) => {
                    if (err) {
                        console.log('PUT /orders/:orderid Error:', err.message);
                        done(err);
                    } else {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('message').eql('Order updated successfully!');
                        res.body.data.should.have.property('note').eql('Updated test order');
                        res.body.data.should.have.property('cost').eql(500);
                        done();
                    }
                });
        });
    });

    describe("DELETE /devcamp-shop24h/orders/:orderid", () => {
        before(async () => {
            // Create an order for this test
            const order = new Order({
                orderDate: new Date(),
                shippedDate: null,
                note: "Test order",
                orderDetails: [orderDetailId],
                cost: 450
            });
            const savedOrder = await order.save();
            orderId = savedOrder._id;
        });

        it("Should delete an order by ID", async () => {
            console.log(`Starting delete test for orderId: ${orderId}`);

            const res = await chai.request(server)
                .delete(`/devcamp-shop24h/orders/${orderId}`);

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('Order deleted successfully!');

            // Verify that the order was actually deleted
            const order = await Order.findById(orderId);
            should.not.exist(order);

            console.log(`Completed delete test for orderId: ${orderId}`);
        });
    });
});
