const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const mongoose = require('mongoose');
const OrderDetail = require('../app/models/orderDetail.model');
const Product = require('../app/models/product.model');
const ProductType = require('../app/models/productType.model');

const should = chai.should();

chai.use(chaiHttp);

describe("Test Devcamp Shop 24h - OrderDetail API", () => {
    let productId;
    let orderDetailId;

    // Ensure the database is clean before running tests
    before(async () => {
        try {
            await mongoose.connect('mongodb://127.0.0.1:27017/devcamp-shop24h-backend', {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            });

            mongoose.connection.on('error', (error) => {
                console.error('MongoDB connection error:', error);
            });

            console.log('MongoDB connected successfully.');

            await OrderDetail.deleteMany({});
            await Product.deleteMany({});
            await ProductType.deleteMany({});

            // Create a dummy ProductType
            const dummyProductType = new ProductType({
                name: "Electronics",
                description: "Electronic devices"
            });
            const savedProductType = await dummyProductType.save();

            console.log('Dummy product type created:', savedProductType);

            // Create a dummy Product
            const dummyProduct = new Product({
                name: "Smartphone",
                description: "Latest model smartphone",
                imageUrl: "http://example.com/smartphone.jpg",
                buyPrice: 500,
                promotionPrice: 450,
                amount: 100,
                type: savedProductType._id
            });
            const savedProduct = await dummyProduct.save();
            productId = savedProduct._id;

            console.log('Dummy product created:', savedProduct);
            console.log('Product ID:', productId);

        } catch (error) {
            console.error('Error in before hook:', error);
        }
    });

    after(async () => {
        // Disconnect from the database after all tests
        await mongoose.connection.close();
    });

    afterEach(async () => {
        // Delete all OrderDetails after each test
        await OrderDetail.deleteMany({});
    });

    describe("POST /devcamp-shop24h/orderdetails", () => {
        it("Should create a new order detail", (done) => {
            chai.request(server)
                .post("/devcamp-shop24h/orderdetails")
                .send({
                    name: "Smartphone",
                    productid: productId.toString(),
                    quantity: 2
                })
                .end((err, res) => {
                    if (err) {
                        console.log('POST /orderdetails Error:', err.message);
                        done(err);
                    } else {
                        res.should.have.status(201);
                        res.body.should.be.a('object');
                        res.body.should.have.property('message').eql('Order detail created successfully!');
                        res.body.should.have.property('data');
                        res.body.data.should.have.property('quantity').eql(2);
                        res.body.data.should.have.property('product');
                        res.body.data.product.should.have.property('name').eql('Smartphone');
                        done();
                    }
                });
        });
    });

    describe("GET /devcamp-shop24h/orderdetails", () => {
        it("Should return an array of all order details", (done) => {
            chai.request(server)
                .get("/devcamp-shop24h/orderdetails")
                .end((err, res) => {
                    if (err) {
                        console.log('GET /orderdetails Error:', err.message);
                        done(err);
                    } else {
                        res.should.have.status(200);
                        res.body.data.should.be.a('array');
                        res.body.should.have.property("message").eql("Order details retrieved successfully!");
                        done();
                    }
                });
        });
    });

    describe("GET /devcamp-shop24h/orderdetails/:orderdetailid", () => {
        before(async () => {
            // Create an order detail for this test
            const orderDetail = new OrderDetail({
                product: productId,
                quantity: 3
            });
            const savedOrderDetail = await orderDetail.save();
            orderDetailId = savedOrderDetail._id;
        });

        it("Should return an order detail by ID", (done) => {
            chai.request(server)
                .get(`/devcamp-shop24h/orderdetails/${orderDetailId}`)
                .end((err, res) => {
                    if (err) {
                        console.log('GET /orderdetails/:orderdetailid Error:', err.message);
                        done(err);
                    } else {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('quantity').eql(3);
                        res.body.should.have.property('product');
                        res.body.product.should.have.property('name').eql('Smartphone');
                        done();
                    }
                });
        });
    });

    describe("PUT /devcamp-shop24h/orderdetails/:orderdetailid", () => {
        before(async () => {
            // Create an order detail for this test
            const orderDetail = new OrderDetail({
                product: productId,
                quantity: 3
            });
            const savedOrderDetail = await orderDetail.save();
            orderDetailId = savedOrderDetail._id;
        });

        it("Should update an order detail by ID", (done) => {
            chai.request(server)
                .put(`/devcamp-shop24h/orderdetails/${orderDetailId}`)
                .send({
                    quantity: 5
                })
                .end((err, res) => {
                    if (err) {
                        console.log('PUT /orderdetails/:orderdetailid Error:', err.message);
                        done(err);
                    } else {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('message').eql('Order detail updated successfully!');
                        res.body.data.should.have.property('quantity').eql(5);
                        done();
                    }
                });
        });
    });

    describe("DELETE /devcamp-shop24h/orderdetails/:orderdetailid", () => {
        before(async () => {
            // Create an order detail for this test
            const orderDetail = new OrderDetail({
                product: productId,
                quantity: 3
            });
            const savedOrderDetail = await orderDetail.save();
            orderDetailId = savedOrderDetail._id;
        });

        it("Should delete an order detail by ID", async () => {
            console.log(`Starting delete test for orderDetailId: ${orderDetailId}`);

            const res = await chai.request(server)
                .delete(`/devcamp-shop24h/orderdetails/${orderDetailId}`);

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('Order detail deleted successfully!');

            // Verify that the order detail was actually deleted
            const orderDetail = await OrderDetail.findById(orderDetailId);
            should.not.exist(orderDetail);

            console.log(`Completed delete test for orderDetailId: ${orderDetailId}`);
        });
    });
});
