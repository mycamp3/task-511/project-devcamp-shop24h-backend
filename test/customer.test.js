const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const mongoose = require('mongoose');
const Customer = require('../app/models/customer.model');
const Order = require('../app/models/order.model');

const should = chai.should();

chai.use(chaiHttp);

describe("Test Devcamp Shop 24h - Customer API", () => {
    let customerId;
    let orderId;

    // Ensure the database is clean before running tests
    before(async () => {
        try {
            await mongoose.connect('mongodb://127.0.0.1:27017/devcamp-shop24h-backend', {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            });

            mongoose.connection.on('error', (error) => {
                console.error('MongoDB connection error:', error);
            });

            console.log('MongoDB connected successfully.');

            await Customer.deleteMany({});
            await Order.deleteMany({});

            // Create a dummy Order
            const dummyOrder = new Order({
                orderDate: new Date(),
                shippedDate: null,
                note: "Test order",
                orderDetails: [],  // No OrderDetails needed for this test
                cost: 0
            });
            const savedOrder = await dummyOrder.save();
            orderId = savedOrder._id;

            console.log('Dummy order created:', savedOrder);
            console.log('Order ID:', orderId);

        } catch (error) {
            console.error('Error in before hook:', error);
        }
    });

    after(async () => {
        // Disconnect from the database after all tests
        await mongoose.connection.close();
    });

    afterEach(async () => {
        // Delete all Customers after each test
        await Customer.deleteMany({});
    });

    describe("POST /devcamp-shop24h/customers", () => {
        it("Should create a new customer", (done) => {
            chai.request(server)
                .post("/devcamp-shop24h/customers")
                .send({
                    fullName: "John Doe",
                    phone: "1234567890",
                    email: "johndoe@example.com",
                    address: "123 Elm Street",
                    city: "Anytown",
                    country: "Countryland",
                    orders: [orderId.toString()]  // Assign the dummy order to the customer
                })
                .end((err, res) => {
                    if (err) {
                        console.log('POST /customers Error:', err.message);
                        done(err);
                    } else {
                        res.should.have.status(201);
                        res.body.should.be.a('object');
                        res.body.should.have.property('message').eql('Customer created successfully!');
                        res.body.should.have.property('data');
                        res.body.data.should.have.property('fullName').eql('John Doe');
                        res.body.data.should.have.property('email').eql('johndoe@example.com');
                        res.body.data.should.have.property('orders').with.lengthOf(1);
                        done();
                    }
                });
        });
    });

    describe("GET /devcamp-shop24h/customers", () => {
        it("Should return an array of all customers", (done) => {
            chai.request(server)
                .get("/devcamp-shop24h/customers")
                .end((err, res) => {
                    if (err) {
                        console.log('GET /customers Error:', err.message);
                        done(err);
                    } else {
                        res.should.have.status(200);
                        res.body.data.should.be.a('array');
                        res.body.should.have.property("message").eql("Customers retrieved successfully!");
                        done();
                    }
                });
        });
    });

    describe("GET /devcamp-shop24h/customers/:customerid", () => {
        before(async () => {
            // Create a customer for this test
            const customer = new Customer({
                fullName: "Jane Smith",
                phone: "0987654321",
                email: "janesmith@example.com",
                address: "456 Oak Avenue",
                city: "Othertown",
                country: "Countryland",
                orders: [orderId]
            });
            const savedCustomer = await customer.save();
            customerId = savedCustomer._id;
        });

        it("Should return a customer by ID", (done) => {
            chai.request(server)
                .get(`/devcamp-shop24h/customers/${customerId}`)
                .end((err, res) => {
                    if (err) {
                        console.log('GET /customers/:customerid Error:', err.message);
                        done(err);
                    } else {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('fullName').eql('Jane Smith');
                        res.body.should.have.property('orders').with.lengthOf(1);
                        done();
                    }
                });
        });
    });

    describe("PUT /devcamp-shop24h/customers/:customerid", () => {
        before(async () => {
            // Create a customer for this test
            const customer = new Customer({
                fullName: "Emily Davis",
                phone: "1112233445",
                email: "emilydavis@example.com",
                address: "789 Pine Road",
                city: "Newtown",
                country: "Countryland",
                orders: [orderId]
            });
            const savedCustomer = await customer.save();
            customerId = savedCustomer._id;
        });

        it("Should update a customer by ID", (done) => {
            chai.request(server)
                .put(`/devcamp-shop24h/customers/${customerId}`)
                .send({
                    fullName: "Emily Johnson",
                    phone: "0912123456",
                    email: "test111@gmail.com",
                    city: "Oldtown",
                })
                .end((err, res) => {
                    if (err) {
                        console.log('PUT /customers/:customerid Error:', err.message);
                        done(err);
                    } else {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('message').eql('Customer updated successfully!');
                        res.body.data.should.have.property('fullName').eql('Emily Johnson');
                        res.body.data.should.have.property('phone').eql('0912123456');
                        res.body.data.should.have.property('city').eql('Oldtown');
                        done();
                    }
                });
        });
    });

    describe("DELETE /devcamp-shop24h/customers/:customerid", () => {
        before(async () => {
            // Create a customer for this test
            const customer = new Customer({
                fullName: "Michael Brown",
                phone: "2233445566",
                email: "michaelbrown@example.com",
                address: "202 Birch Street",
                city: "Sometown",
                country: "Countryland",
                orders: [orderId]
            });
            const savedCustomer = await customer.save();
            customerId = savedCustomer._id;
        });

        it("Should delete a customer by ID", async () => {
            console.log(`Starting delete test for customerId: ${customerId}`);

            const res = await chai.request(server)
                .delete(`/devcamp-shop24h/customers/${customerId}`);

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('Customer deleted successfully!');

            // Verify that the customer was actually deleted
            const customer = await Customer.findById(customerId);
            should.not.exist(customer);

            console.log(`Completed delete test for customerId: ${customerId}`);
        });
    });
});
